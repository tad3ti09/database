-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 02, 2020 at 12:18 PM
-- Server version: 10.1.45-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `researc1_himatif1`
--

-- --------------------------------------------------------

--
-- Table structure for table `bph`
--

CREATE TABLE `bph` (
  `bph_id` int(11) NOT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `mahasiswa_id` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bph`
--

INSERT INTO `bph` (`bph_id`, `periode_id`, `mahasiswa_id`, `jabatan`) VALUES
(3, 1, 1, 'Ketua'),
(6, 1, 23, 'Sekretaris'),
(15, 7, 15, 'Ketua'),
(16, 7, 16, 'waket'),
(17, 7, 19, 'bendahara'),
(18, 7, 18, 'bendahara'),
(19, 7, 17, 'sekretaris'),
(20, 1, 14, 'bendahara'),
(21, 8, 13, 'divac'),
(22, 4, 23, 'sekretaris'),
(23, 4, 24, 'bendahara');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `kegiatan_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `nama_kegiatan` varchar(100) DEFAULT NULL,
  `waktu_kegiatan` date DEFAULT NULL,
  `tempat_kegiatan` varchar(100) DEFAULT NULL,
  `deskripsi_kegiatan` text,
  `foto_kegiatan` varchar(255) NOT NULL,
  `tanggal_berakhir` date NOT NULL,
  `status_kegiatan` varchar(100) NOT NULL,
  `penulis` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`kegiatan_id`, `bph_id`, `periode_id`, `nama_kegiatan`, `waktu_kegiatan`, `tempat_kegiatan`, `deskripsi_kegiatan`, `foto_kegiatan`, `tanggal_berakhir`, `status_kegiatan`, `penulis`) VALUES
(1, 3, 1, 'Perpisahan 2016', '2020-01-14', 'Holbung', '			Kegiatan dilaksanakan di Open Teather bersama semua angkatan									', 'IMG_2995.JPG', '2020-06-24', 'Terlaksanakan', 'Hernan'),
(26, 3, 1, 'Wisuda 2017', '2020-09-09', 'Institut Teknologi Del', '						Wisuda Angkatan 2017 D3 Teknologin Informasi 								', 'IMG_3039.JPG', '2020-09-09', 'Terlaksanakan', 'Hernan'),
(29, 3, 1, 'Refreshing Day', '2020-07-16', 'Tambunan', 'Untuk Mempererat Ikatan Persaudaraan ', 'c.jpeg', '2020-08-17', 'Terlaksanakan', 'Hernan Crespo Panjaitan'),
(32, 3, 1, 'Ramah Tamah 2020', '2020-07-28', 'Open Theater', 'Penyambutan angkatan 2020', 'del.jpg', '2020-07-28', 'Belum Terlaksanakan', 'Hernan Crespo Panjaitan');

-- --------------------------------------------------------

--
-- Table structure for table `kuisioner_kegiatan`
--

CREATE TABLE `kuisioner_kegiatan` (
  `kuisioner_id` int(11) NOT NULL,
  `kegiatan_id` int(11) NOT NULL,
  `mahasiswa_id` int(11) NOT NULL,
  `jab1` varchar(225) NOT NULL,
  `jab2` varchar(225) NOT NULL,
  `jab3` varchar(225) NOT NULL,
  `jab4` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuisioner_kegiatan`
--

INSERT INTO `kuisioner_kegiatan` (`kuisioner_id`, `kegiatan_id`, `mahasiswa_id`, `jab1`, `jab2`, `jab3`, `jab4`) VALUES
(1, 1, 1, 'Tidak', 'Tidak', 'Ya', 'asdf'),
(2, 26, 1, 'Ya', 'Tidak', 'Ya', 'bagus'),
(3, 1, 1, 'Ya', 'Ya', 'Ya', 'keren'),
(4, 26, 1, 'Ya', 'Tidak', 'Ya', 'mantap'),
(5, 26, 1, 'Ya', 'Ya', 'Tidak', 'sangat bermanfaat'),
(6, 1, 1, 'Ya', 'Ya', 'Ya', 'Kegiatan yang bermanfaat'),
(7, 1, 1, 'Ya', 'Ya', 'Ya', 'Kegiatan bermanfaat'),
(8, 29, 1, 'Ya', 'Ya', 'Ya', 'kegiatan bermanfaat'),
(9, 29, 1, 'Ya', 'Ya', 'Ya', 'haha'),
(10, 30, 1, 'Ya', 'Ya', 'Ya', 'bermanfaat'),
(11, 30, 1, 'Ya', 'Ya', 'Ya', 'bermanfaat'),
(12, 29, 1, 'Ya', 'Ya', 'Ya', 'keren'),
(13, 29, 1, 'Ya', 'Ya', 'Ya', 'keren'),
(14, 32, 1, 'Ya', 'Ya', 'Ya', 'keren '),
(15, 1, 1, 'Ya', 'Ya', 'Ya', 'fhh'),
(16, 32, 1, 'Tidak', 'Ya', 'Ya', 'keren'),
(17, 32, 1, 'Tidak', 'Ya', 'Ya', 'keren2'),
(18, 26, 1, 'Ya', 'Tidak', 'Ya', 'keren'),
(19, 29, 1, 'Ya', 'Ya', 'Ya', 'wadaw'),
(20, 32, 1, 'Ya', 'Ya', 'Ya', 'wadaw'),
(21, 29, 1, 'Ya', 'Tidak', 'Tidak', 'wadaw'),
(22, 1, 1, 'Tidak', 'ya', 'ya', 'mantap'),
(23, 1, 1, 'Tidak', 'ya', 'ya', 'mantap');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `mahasiswa_id` int(11) NOT NULL,
  `nama` text,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `nim` varchar(100) DEFAULT NULL,
  `kelas` text,
  `angkatan` varchar(100) DEFAULT NULL,
  `alamat` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `status_mahasiswa` varchar(225) NOT NULL,
  `tempat_lahir` varchar(225) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `role` varchar(100) NOT NULL,
  `foto_mahasiswa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`mahasiswa_id`, `nama`, `username`, `password`, `nim`, `kelas`, `angkatan`, `alamat`, `email`, `status_mahasiswa`, `tempat_lahir`, `tanggal_lahir`, `role`, `foto_mahasiswa`) VALUES
(1, 'Hernan Crespo Panjaitan', 'hernan', '6ec262620c6dccd8c5a9de049a22fc38', '11317056', '32TI2', '2017', '												JL.Bahkora II Bawah PematangSiantar																', 'if318056@itdel', 'aktif', 'DolokSanggul', '1999-07-19', 'admin', 'IMG-20180517-WA0014.jpg'),
(13, 'Romauli Feronica Siregar', 'romauli', 'a4d5ff8eaa981778ed19953516391e63c20d793a', '11317065', '33TI2', '2017', '			Sidikalang										', 'if317065@itdel', 'aktif', 'Parongil', '1998-07-12', 'user', 'roma.jpeg'),
(14, 'Layla Hafni Ainun Hutasuhut', 'layla', 'c08a6ee5d602cd7916c8b7d3cef8af98', '11317061', '33TI2', '2017', 'Sigumpar																', 'if317061@students.del.ac.id', 'aktif', 'Lumban Binanga', '1999-07-22', 'admin', 'lay.jpeg'),
(15, 'Hagai Sitanggang', 'hagai', 'e8137a7d3b274eafbba97ae37491bd55', '11317048', '33TI2', '2017', '			Pematang Siantar						', 'if317048@students.del.ac.id', 'aktif', 'Pematang Siantar', '1999-07-19', 'user', 'hagaaaa.jpg'),
(16, 'Josua Barimbing', 'Josua', '9974cb2e89f523f9472699e2cdc6ec01', '11318060', '32TI2', '2018', '						Balige				', 'if318060@students.del.ac.id', 'aktif', 'Balige', '2000-08-07', 'user', 'joshua.png'),
(17, 'Wenny Siagian', 'wenny', '0635695ec843adbfd3a7940e4bf3c01b', '11317054', '33TI2', '2017', 'Silaen										', 'if317054@students.del.ac.id', 'aktif', 'Silaen', '1999-07-19', 'user', 'wen.JPG'),
(18, 'Mutiara Simamora', 'mutiara', 'f58e35e214c208599694b9d9b3b43a51', '11317050', '33TI2', '2017', 'Medan										', 'if317050@students.del.ac.id', 'aktif', 'Medan', '1999-05-20', 'user', 'muti.JPG'),
(19, 'Ayu Lumbantobing', 'ayu', '29c65f781a1068a41f735e1b092546de', '11317060', '33TI2', '2017', 'Tarutung								', 'if317060@students.del.ac.id', 'aktif', 'Tarutung', '1998-07-19', 'admin', 'ayu.jpg'),
(20, 'Sylvia Kornelina Sihombing', 'sylvia', 'd7bf862a7731a9bd5fed111ccb9a4ccc', '11317022', '33TI1', '2017', '															Tarutung										', 'if317022@students.del.ac.id', 'aktif', 'Tarutung', '1998-12-09', 'user', 'vi.JPG'),
(21, 'Gabriel', 'gaba', '$2y$10$H2eJ1PM8HylGNteRP1e2Ue0s474CyMKTo0fJ4B6REL14m/rGxFnNy', '11417020', '43TRPL', '2017', 'Balige', 'if417020@itdel', 'aktif', 'Siantar', '2020-07-15', 'admin', '421c8108cfac759d262052bf103b71ea.jpg'),
(22, 'januar Tampu', 'januar', '$2y$10$aFpIHdEnhE/Eg/vlgZ297.wqIPda9m.phBIKAl3VE4eVN9rzJOmxq', '11317001', '33TI1', '2017', 'Sibolga', '11317001@itdel', 'aktif', 'Sibolga', '2020-07-18', 'admin', 'WhatsApp Image 2020-05-18 at 10.26.29.jpeg'),
(23, 'Romauli Feronica Siregar', 'roma12', '$2y$10$9ohtle45W0wsowXuU4Ro7eq0yAI2SguL1yQKuCETKPp72wl5eq7ZG', '11317065', '33TI2', '2017', 'goal', 'if317065@students.del.ac.id', 'aktif', 'Parongil', '1998-09-19', 'admin', 'Roma.jpg'),
(24, 'Layla Hafni Ainun Hutasuhut', 'layla', '$2y$10$d4wpzix0FIi0HvSpMYiH2.OiC9tiNofX9ZzZ76tdvxngQrg6DKifK', '11317061', '33TI2', '2017', 'Sigumpar', 'if317061@itdel', 'aktif', 'Lumban Binanga', '1999-07-22', 'admin', 'LHA.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `detail_pengumuman` text,
  `tanggal_akhir_pengumuman` date DEFAULT NULL,
  `judul_pengumuman` varchar(225) NOT NULL,
  `foto_pengumuman` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`pengumuman_id`, `bph_id`, `periode_id`, `detail_pengumuman`, `tanggal_akhir_pengumuman`, `judul_pengumuman`, `foto_pengumuman`) VALUES
(3, NULL, NULL, 'Dear Mahasiswa,\r\n\r\nBerikut ini kami sampaikan Surat WR I tentang Batas Akhir Pelaksanaan Tugas Akhir Mahasiswa IT Del Tahun Ajaran 2019/2020 di Institut Teknologi Del.\r\n\r\nDemikian Informasi ini kami sampaikan.	\r\n							', '2020-01-29', '[Surat Edaran WR I] Batas Akhir Pelaksanaan Tugas Akhir Mahasiswa IT Del Tahun Ajaran 2019/2020 di Institut Teknologi Del', 'tiga.png'),
(4, 3, 1, 'Seleksi untuk kompetisi		', '2020-07-24', 'Seleksi HIMATIF Coding Challenge', 'a.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `periode_id` int(11) NOT NULL,
  `periode_awal` varchar(100) DEFAULT NULL,
  `periode_akhir` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`periode_id`, `periode_awal`, `periode_akhir`, `status`) VALUES
(1, '2016', '2020', 1),
(3, '2019', '2022', 1),
(4, '2017', '2020', 1),
(7, '2018', '2021', 1),
(8, '2020', '2021', 1);

-- --------------------------------------------------------

--
-- Table structure for table `uang_kas`
--

CREATE TABLE `uang_kas` (
  `uang_kas_id` int(11) NOT NULL,
  `bph_id` int(11) DEFAULT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `jumlah_bayar` varchar(225) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_berakhir` date NOT NULL,
  `deskripsi_pembayaran` text,
  `status_pembayaran` varchar(50) DEFAULT NULL,
  `mahasiswa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uang_kas`
--

INSERT INTO `uang_kas` (`uang_kas_id`, `bph_id`, `periode_id`, `jumlah_bayar`, `tanggal_bayar`, `tanggal_berakhir`, `deskripsi_pembayaran`, `status_pembayaran`, `mahasiswa_id`) VALUES
(5, 3, 1, '20.000', '2020-07-02', '2020-08-02', 'Bayar Juli', 'Lunas', 20),
(7, 3, 1, 'Rp.678.000', '2020-07-17', '2020-08-17', 'Uang kas Bulan Juli', 'Belum Bayar', 18),
(11, 3, 1, '30000', '2020-07-21', '2020-07-23', 'Uang kas Bulan Juli', 'Sudah Bayar', 19),
(12, 3, 1, '30000', '2020-07-14', '2020-07-31', 'Uang Kas Juli', 'Sudah Bayar', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bph`
--
ALTER TABLE `bph`
  ADD PRIMARY KEY (`bph_id`),
  ADD KEY `periode_id` (`periode_id`),
  ADD KEY `mahasiswa_id` (`mahasiswa_id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`kegiatan_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`);

--
-- Indexes for table `kuisioner_kegiatan`
--
ALTER TABLE `kuisioner_kegiatan`
  ADD PRIMARY KEY (`kuisioner_id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`mahasiswa_id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`pengumuman_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`periode_id`);

--
-- Indexes for table `uang_kas`
--
ALTER TABLE `uang_kas`
  ADD PRIMARY KEY (`uang_kas_id`),
  ADD KEY `bph_id` (`bph_id`),
  ADD KEY `periode_id` (`periode_id`),
  ADD KEY `mahasiswa_id` (`mahasiswa_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bph`
--
ALTER TABLE `bph`
  MODIFY `bph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `kegiatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `kuisioner_kegiatan`
--
ALTER TABLE `kuisioner_kegiatan`
  MODIFY `kuisioner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `mahasiswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `periode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `uang_kas`
--
ALTER TABLE `uang_kas`
  MODIFY `uang_kas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bph`
--
ALTER TABLE `bph`
  ADD CONSTRAINT `bph_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`),
  ADD CONSTRAINT `bph_ibfk_2` FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`mahasiswa_id`);

--
-- Constraints for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD CONSTRAINT `kegiatan_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `kegiatan_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`);

--
-- Constraints for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `pengumuman_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`);

--
-- Constraints for table `uang_kas`
--
ALTER TABLE `uang_kas`
  ADD CONSTRAINT `uang_kas_ibfk_1` FOREIGN KEY (`bph_id`) REFERENCES `bph` (`bph_id`),
  ADD CONSTRAINT `uang_kas_ibfk_2` FOREIGN KEY (`periode_id`) REFERENCES `periode` (`periode_id`),
  ADD CONSTRAINT `uang_kas_ibfk_3` FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`mahasiswa_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
